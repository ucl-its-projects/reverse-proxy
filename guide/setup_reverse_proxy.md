author:            19A ITS1
summary:           Setup a reverse proxy
id:                setup_reverse_proxy
categories:        NGINX reverse proxy
environments:      Virtual machines
status:            wip
feedback link:     MON@UCL.DK
analytics account: Always 0

# Setup a reverse proxy using Nginx

## Introduction

Nginx has the capability of being a reverse proxy. More info in the [official docs](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)

There are two way of doing it

1. by requesting a subdirectory
2. by requesting a host


We will set it up in a vmware environment.

###  Request a subdirectory
To pass a request to an HTTP proxied server, the proxy_pass directive is specified inside a location. For example:

```
location /Web A/path/ {
    proxy_pass http://www.example.com/link/;
}
```


### Request a host
By default, NGINX redefines two header fields in proxied requests, “Host” and “Connection”,
and eliminates the header fields whose values are empty strings. “Host” is set to the $proxy_host variable, and “Connection” is set to close.

To change these setting, as well as modify other header fields, use the proxy_set_header directive.
This directive can be specified in a location or higher. It can also be specified in a particular server context or in the http block. For example:

```
location /Web A/path/ {
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_pass http://localhost:8000;
}
```

In this configuration the “Host” field is set to the $host variable.

To prevent a header field from being passed to the proxied server, set it to an empty string as follows:

```
location /Web A/path/ {
    proxy_set_header Accept-Encoding "";
    proxy_pass http://localhost:8000;
}
```

### Useful links

Nginx has a lot of [official documentation](https://docs.nginx.com/nginx/admin-guide/).


## Basic design

In order to demonstrate reverse proxying, we need a basic design with multiple internal webservers.

![Design Diagram](img/2019-10-04_-_Exercise_2_Design.png)


## Configuring the internal web servers

The internal webserver are basic webservers running nginx.

We will clone them from the basic debian minimal installation

```
#!/bin/bash

apt install nginx

echo "source /etc/network/interfaces.d/*" > /etc/network/inetfaces
echo "overwritten the file"
echo "" >> /etc/network/interfaces

echo "auto lo" >> /etc/network/interfaces
echo "iface lo inet loopback" >> /etc/network/interfaces
echo "wrote the loopback network interface"
echo "" >> /etc/network/interfaces

echo "allow-hotplug ens33" >> /etc/network/interfaces
echo "iface ens33 inet static" >> /etc/network/interfaces

server=$1

ipA=192.168.11.50
ipB=192.168.11.51

if [ $server == "a"]
then
    echo "  address $ipA/24" >> /etc/network/interfaces
fi

if [ server == "b"]
then
    echo "  address $ipB/24" >> /etc/network/interfaces
fi

echo "  gateway 192.168.11.2" >> /etc/network/interfaces
echo "configured ip, netmask and gateway"

echo "nameserver 192.168.11.2" > /etc/resolv.conf
echo "configrued DNS"
```

* add files or copy the code needed here
* Describe how to test

## Configuring the reverse proxy

The reverse proxy will have the most complex configuration.

The configuration has two logical parts: the directory and the hostname approches.

1. In order for the configurations to work, we update `/etc/hosts` on the reverse proxy server, so it can resolv the host names, ie.

    ```
    127.0.0.1	localhost
    127.0.1.1	revproxy

    # The following lines are desirable for IPv6 capable hosts
    ::1     localhost ip6-localhost ip6-loopback
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters

    192.168.36.51 WebA
    192.168.36.52 WebB
    ```

    The default site configuration is in `/etc/nginx/sites-available`. We want to add the entries related to the proxied server to be a part of the default server definition.

1. Disable the old default config using `rm /etc/nginx/sites-enabled/default`.



## Reverse proxy - Subdirectory approach



2. Create a new config file called `/etc/nginx/sites-availble/proxy-dir`.

    ```
    server {
    	listen 80 default_server;
    	listen [::]:80 default_server;

    	root /var/www/html;
    	index index.html index.htm index.nginx-debian.html;

    	server_name revproxy;

      location /weba {
          proxy_pass http://weba/;
      }
      location /webb {
          proxy_pass http://webb/;
      }
      location / {
    		# First attempt to serve request as file, then
    		# as directory, then fall back to displaying a 404.
    		try_files $uri $uri/ =404;
    	}
    }
    ```

3. Enable it by create a symlink from sites-available to sites-enabled, ie. `cd /etc/nginx/sites-enabled; ln -s ../sites-available/proxy-dir .`

4. Check nginx config; `nginx -t`

    Errors like `nginx: [emerg] host not found in upstream "weba" in /etc/nginx/sites-enabled/proxy-dir:16` could mean that nginx could not resolve the server that is referenced. Double hostname in config file and `/etc/hosts.`


5. Apply the new config; `service nginx reload`

6. test that it works using a browser and navigate to the ip of the reverse proxy and access `http://<host>/`, `http://<host>/weba` and `http://<host>/webb`

##  Reverse proxy - hostname approach

1. Create a new config file called `/etc/nginx/sites-availble/weba.conf`

```
server {
  listen 80 default_server;
  listen [::]:80 default_server;

  server_name weba;

  location / {
      proxy_pass http://weba/;
  }
}
```


2. Enable it by create a symlink from sites-available to sites-enabled, ie. `cd /etc/nginx/sites-enabled; ln -s ../sites-available/proxy-dir .`

4. Check nginx config; `nginx -t`

5. Apply the new config; `service nginx reload`

5. Ensure that `weba` resolves to the reverse proxy ip address. This is set up either in the hosts file or the DNS server associated with the browser. **Not** on the reverse proxy.

6. test that it works using a browser and navigate to the ip of the reverse proxy and access `http://weba/`


## Convenient links

TODO:
* add link to complete (annotated) config file
* add link to test script

## Automate

TODO:
* we need a script that automatically deploys a vmware VM
* we need a script that provisions the simple web servers
* we need a script that provisions the reverse proxy
* does gitlab CI fit with this somehow?
